﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class which generates asteroids following a collection of parameters.
/// </summary>
public class AsteroidGeneratorManager : MonoBehaviour, IListener
{

    #region Variables    
    /// <summary>
    /// Quantity of spawning points. This sample game is designed for 4.
    /// </summary> 
    private const int SPAWNING_POINTS_SIZE = 4;
    /// <summary>
    /// Lower parameter for calculating the asteroid quantity and position.
    /// </summary>
    private const int GENERATOR_CODE_LOWER_LIMIT = 1;
    /// <summary>
    /// Upper parameter for calculating the asteroid quantity and position.
    /// </summary>
    private const int GENERATOR_CODE_UPPER_LIMIT = (1 << SPAWNING_POINTS_SIZE) -1;

    [Header("Position")]
    [Tooltip("The entry points for the asteroids. It's limited to 4.")]
    public Transform [] spawingPoints = new Transform[SPAWNING_POINTS_SIZE];

    [Header("Velocity")]
    [Range(1, 5)]
    [Tooltip("The base velocity for a new asteroid.")]
    public float baseSpeed;
    [Range(1, 5)]
    [Tooltip("The velocity range to be added randomly to new asteroids.")]
    public float addedSpeedRange;
    [Range(3, 10)]
    [Tooltip("The added velocity the asteroids have each time it's speeded up.")]
    public float fasterSpeed;
    [Range(-5, 0)]
    [Tooltip("Lower limit for rotation speed.")]
    public float rotationLowerLimit;
    [Range(0, 5)]
    [Tooltip("Upper limit for rotation speed.")]
    public float rotationUpperLimit;

    [Header("Super Asteroid")]
    [Tooltip("Quantity to be added to the percentage of spawning a super asteroid each generation.")]
    [Range(10, 25)]
    public float addedPercentage;
    [Tooltip("Multiplier for the super asteroid velocity from normal asteroid velocity.")]
    [Range(0.5f, 1)]
    public float velocityDecrement;
    [Tooltip("Amount of distance for the asteroids which are generated after the super asteroid split.")]
    [Range(0.5f, 1)]
    public float spacing;

    [Header("Time")]
    [Tooltip("Time interval needed to spawn again the asteroids.")]
    [Range(1, 5)]
    public float timeInterval;    

    /// <summary>
    /// Actual base speed.
    /// </summary>
    float actualSpeed;

    /// <summary>
    /// The % to spawn a super asteroid.
    /// </summary>
    float possibilitySuperAsteroid;

    /// <summary>
    /// Flag for knowing if player is active.
    /// </summary>
    bool isPlaying;

    /// <summary>
    /// Time passed for each spawning intervals.
    /// </summary>
    float deltaTime;
    #endregion

    #region UnityMethods
    /// <summary>
    /// As the class is created, local variables are setted for later use.
    /// </summary>
    void Awake()
    {
        Reset();
    }

    /// <summary>
    /// The class gets subscribed to a variety of events.
    /// </summary>
    void Start()
    {                                                            
        EventManager.Instance.AddListener(EVENT_TYPE.START_GAMEPLAY, this);
        EventManager.Instance.AddListener(EVENT_TYPE.PAUSE_GAMEPLAY, this);
        EventManager.Instance.AddListener(EVENT_TYPE.RESUME_GAMEPLAY, this);
        EventManager.Instance.AddListener(EVENT_TYPE.END_GAMEPLAY, this);
        EventManager.Instance.AddListener(EVENT_TYPE.SPEED_UP, this);
        EventManager.Instance.AddListener(EVENT_TYPE.SUPER_ASTEROID_SPLIT, this);
    }

    /// <summary>
    /// If the player is playing, the generation process takes place.
    /// </summary>
    void Update()
    {
        if (isPlaying)
        {
            //If the time interval has already ended, a new wave of asteroids is generated.
            if((deltaTime += Time.deltaTime) >= timeInterval)
            {
                //The possibility of making a super asteroid in the next generation is setted to 0.
                deltaTime = 0;
                //The first phase indicates if the manager will create a super asteroid or not.
                if (UnityEngine.Random.Range(0, 100) < possibilitySuperAsteroid)
                {
                    possibilitySuperAsteroid = 0;
                    //A new velocity is calculated with the super asteroid velocity decrement.
                    float speed = (actualSpeed * velocityDecrement) + UnityEngine.Random.Range(0, addedSpeedRange);
                    //The super asteroid will appear in one of the 2 central spawn points.
                    if (UnityEngine.Random.value < 0.5)
                    {
                        GenerateAsteroid(spawingPoints[1].position, speed, true);   
                    }
                    else
                    {
                        GenerateAsteroid(spawingPoints[2].position, speed, true);   
                    }
                }
                else
                {
                    //The possibility of making a super asteroid in the next generation is increased.                                  
                    possibilitySuperAsteroid += addedPercentage;
                    //A random number is generated and its bits are used for calculating the quantity and position. For instance, 9 is '1001' in binary, so there will be new asteroids in spawn points '0' and '3'.
                    int generatorCode = UnityEngine.Random.Range(GENERATOR_CODE_LOWER_LIMIT, GENERATOR_CODE_UPPER_LIMIT);
                    //Every bit is checked for spawning or not new asteroids.
                    for (int i = 0; i < SPAWNING_POINTS_SIZE; i++)
                    {
                        if((generatorCode & 1) != 0)
                        {
                            float speed = actualSpeed + UnityEngine.Random.Range(0, addedSpeedRange);  
                            GenerateAsteroid(spawingPoints[i].position, speed);
                        }
                        generatorCode = generatorCode >> 1;
                    }
                }                          
            }
        }
    }

    /// <summary>
    /// Additional method to ensure the spawning points array is always the SPAWNING_POINTS_SIZE method variable value.
    /// </summary>
    void OnValidate()
    {
        if (spawingPoints.Length != SPAWNING_POINTS_SIZE)
        {
            Debug.LogWarning("Don't change the 'spawningPoints' size!");
            Array.Resize(ref spawingPoints, SPAWNING_POINTS_SIZE);
        }
    }
    #endregion

    #region AddedMethods   
    /// <summary>
    /// Interface iListener method implementation.
    /// </summary>
    /// <param name="Event_Type">Event type posted.</param>
    /// <param name="Sender">Component who post the event.</param>
    /// <param name="Param">Additional information for the event.</param>
    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, UnityEngine.Object Param = null)
    {
        switch (Event_Type)
        {
            case EVENT_TYPE.START_GAMEPLAY:
                Reset();
                isPlaying = true;
                break;
            case EVENT_TYPE.PAUSE_GAMEPLAY:
                isPlaying = false;
                break;
            case EVENT_TYPE.RESUME_GAMEPLAY:
                isPlaying = true;
                break;
            case EVENT_TYPE.END_GAMEPLAY:
                AsteroidPooling.Instance.DeactivateAllObjects();
                isPlaying = false;
                break;
            case EVENT_TYPE.SPEED_UP:
                SpeedUp();
                break;
            case EVENT_TYPE.SUPER_ASTEROID_SPLIT:
                SplitAsteroid(Param);  
                break;
        }
    }

    /// <summary>
    /// Local variables ready for a new game.
    /// </summary>
    void Reset()
    {
        actualSpeed = baseSpeed;
        possibilitySuperAsteroid = 0;
        isPlaying = false;
        deltaTime = -timeInterval;
    }

    /// <summary>
    /// Method called to speed up the velocity of the asteroids.
    /// </summary>
    void SpeedUp()
    {
        actualSpeed += fasterSpeed;
        deltaTime = -timeInterval;
    }

    /// <summary>
    /// This method is used for generating new asteroids.
    /// </summary>
    /// <param name="position">Where to generate it.</param>
    /// <param name="speed">New asteroid's speed.</param>
    /// <param name="super">Is the asteroid a super asteroid?</param>
    void GenerateAsteroid(Vector3 position, float speed, bool super = false)
    {
        GameObject obj;
        if (super)
        {
            obj = AsteroidPooling.Instance.GetPooledSuperAsteroid();
        }
        else
        {
            obj = AsteroidPooling.Instance.GetPooledAsteroid();
        }
        obj.transform.position = position;
        obj.GetComponent<AsteroidController>().Super = super;
        obj.GetComponent<AsteroidController>().ActivateAsteroid(speed, UnityEngine.Random.Range(rotationLowerLimit, rotationUpperLimit));
    }

    /// <summary>
    /// This method generates two new asteroid where the super asteroid is splited.
    /// </summary>
    /// <param name="Param">Data collection with the super asteroid position and speed parameters.</param>
    void SplitAsteroid(UnityEngine.Object Param = null)
    {    
        SuperAsteroidData data = (SuperAsteroidData)Param;
        if(data.Transform == null || data.Speed == null)
        {
            Debug.LogWarning("Super Asteroid is not sending data to the generator manager.");
            return;
        }
        GenerateAsteroid(data.Transform.position + Vector3.right * spacing, data.Speed);
        GenerateAsteroid(data.Transform.position + Vector3.left * spacing, data.Speed);
    }
    #endregion
}